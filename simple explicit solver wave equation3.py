import numpy as np
import pylab as pl

def first_timestep(A_ew,x,deltat,h0,u_j,u_jm1):
    # First timestep in matrix form
    u_j = 0.5*np.dot(A_ew,u_jm1) + deltat*v_I(x)
    # First timestep boundary condition
    u_j[0] = h0[0]; u_j[mx] = h0[mx];
    
    return u_j

def vary_wavespeed_matrix(deltat,deltax):
    delta = (deltat/deltax)**2
    #q_i-0.5 = h(i*deltax - 0.5*deltax)
    diag_m1 = np.zeros(mx+1)
    diag_0 = np.zeros(mx+1)
    diag_1 = np.zeros(mx+1)
    for i in range(0,mx+1):
        diag_m1[i] = delta*h(i*deltax - 0.5*deltax)
        diag_0[i] = 2 - delta*(h(i*deltax + 0.5*deltax) + h(i*deltax - 0.5*deltax))
        diag_1[i] = delta*h(i*deltax + 0.5*deltax)
    A = np.diagflat(diag_m1[1:],-1) + np.diagflat(diag_0) + np.diagflat(diag_m1[:-1],1)
    
    return A

'''unconditionally stable method'''
def solver(h,x,deltax,deltat,h0,mx,mt,u_jp1,u_j,u_jm1):        
    '''new lmbda at the boundaries'''
    # c is the square root of h0
    lmbda = (deltat/deltax)*np.sqrt(h(x))
    #martix
    A_ew = vary_wavespeed_matrix(deltat,deltax)
    #first timestep
    u_j = first_timestep(A_ew,x,deltat,h(x),u_j,u_jm1)
    
    # Solve the PDE: loop over all time points after first time step
    status = "not reached"
    for i in range(2, mt+1):
        #u_jp1 = spsolve(A_iw,(2*u_j + B_iw@u_jm1))
        u_jp1 = np.dot(A_ew,u_j) - u_jm1
        
        # boundary conditions (open BC's)
        tol = h0 + 1e-4
        condition = abs(u_jp1[0])
        
        if condition > tol and status == "not reached":
            status = "reached"
        if tol > condition and status == "reached":
            status = "passed"
        
        if status == "reached" or status == "not reached":
            u_jp1[0] = u_j[0] + (lmbda[1]*u_j[1])-(lmbda[0]*u_j[0]) 
        elif status == "passed":
            u_jp1[0] = u_j[mx]
        
        u_jp1[mx] = u_j[mx] - (lmbda[mx]*u_j[mx]) + (lmbda[mx-1]*u_j[mx-1])
                
        # update u_jm1 and u_j
        u_jm1[:],u_j[:] = u_j[:],u_jp1[:] 
    
    print(status)
    return u_j

'''missing the other unconditionally stable method'''

def hyperbolic_pde(h,L,T,u0,h0,mx,mt):
    # Set up the numerical environment variables
    x = np.linspace(0, L, mx+1)     # gridpoints in space
    t = np.linspace(0, T, mt+1)     # gridpoints in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
    print("deltax=",deltax)
    print("deltat=",deltat)

    # set up the solution variables
    u_jm1 = np.zeros(x.size)        # u at previous time step
    u_j = np.zeros(x.size)          # u at current time step
    u_jp1 = np.zeros(x.size)        # u at next time step
    
    # Set initial condition
    for i in range(0, mx+1):
        u_jm1[i] = u_I(x[i],L)   

    U = solver(h,x,deltax,deltat,h0,mx,mt,u_jp1,u_j,u_jm1)
        
    return x,U
    
'''main function'''
if __name__ == "__main__":
    '''Set problem parameters'''
    L=4       # length of spatial domain
    T= 1    # total time to solve for
    # Set numerical parameters
    mx = 50    # number of gridpoints in space
    mt = 1000    # number of gridpoints in time
    
    '''initial condition and boundary conditions'''   
    #the height of the sea surface
    h0 = 1
    #placement of initial wave and disturbance in the seabed
    placement_ss = 0.3*L; placement_sb = 0.6*L
    #size of the initial disturbance in the sea suface and seabed
    size_ss = 0.1; size_sb = 0.05
    #function for seabed
    def seabed(x):
        return size_sb*np.exp(-((x - placement_sb)**2)/(0.05*L)**2)    
    #the height of the sea bed
    def h(x):
        return 0.5*h0 - seabed(x)
    #the initial wave
    def u_I(x,L):
        # initial displacement
        y = 0.5*h0 + size_ss*np.exp(-((x - placement_ss)**2)/(0.05*L)**2)
        return y
    #the initial wave velocity
    def v_I(x):
        # initial velocity
        y = np.zeros(x.size)
        return y

    '''solving wave equation'''
    x, U = hyperbolic_pde(h,L,T,u_I,0.5*h0,mx,mt) 
    
    '''Plot the sea surface and seabed'''
    pl.plot(x,U+0.5*h0,'r-',label='sea surface')
    xx = np.linspace(0,L,250)
    pl.plot(xx,seabed(xx),'b-',label='seabed')
    pl.xlabel('x')
    pl.ylabel('u(x,'+str(T)+')')
    pl.legend()
    pl.show