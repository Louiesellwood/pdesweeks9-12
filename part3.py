import numpy as np
import pylab as pl
from scipy.sparse import diags
from scipy.sparse.linalg import spsolve

def u_I(x,L):
    # initial temperature distribution
    y = np.sin(np.pi*x/L)
    return y

def u_exact(x,t):
    #The exact solution to this specific problem
    #With an initial temp distribution of sin(pi(x/L))
    y = np.exp(-kappa*(np.pi**2/L**2)*t)*np.sin(np.pi*x/L)
    return y

def nicholson(lmbda,mx,mt,u_jp1,u_j,u_b):
    #Crank-Nicholson 
    A_cn = diags([-(lmbda/2),1+lmbda,-(lmbda/2)],[-1,0,1],shape=(mx-1,mx-1)).toarray()
    B_cn = diags([lmbda/2,1-lmbda,lmbda/2],[-1,0,1],shape=(mx-1,mx-1)).toarray()
    for n in range(1, mt+1):
        # Backward Euler timestep at inner mesh points
        u_jp1[1:-1] = spsolve(A_cn,B_cn.dot(u_j[1:-1]))
        # Boundary conditions
        u_jp1[0] = u_b[0]; u_jp1[mx] = u_b[1]
        # Update u_j
        u_j[:] = u_jp1[:]
        
    return u_j
        
def backwards_Euler(lmbda,mx,mt,u_jp1,u_j,u_b):
    #backwards Euler
    A_be = diags([-lmbda,1+(2*lmbda),-lmbda],[-1,0,1],shape=(mx-1,mx-1))
    
    for n in range(1, mt):
        # Backward Euler timestep at inner mesh points
        u_jp1[1:-1] = spsolve(A_be,u_j[1:-1])   
        # Boundary conditions
        u_jp1[0] = u_b[0]; u_jp1[mx] = u_b[1]
        # Update u_j
        u_j[:] = u_jp1[:]
    
    return u_j    
    
def forwards_Euler(lmbda,mx,mt,u_jp1,u_j,u_b):
    #forwards Euler method
    #Solve the PDE: loop over all time points
    for n in range(1, mt+1):
        # Forward Euler timestep at inner mesh points
        for i in range(1, mx):
            u_jp1[i] = u_j[i] + lmbda*(u_j[i-1] - 2*u_j[i] + u_j[i+1])
            
        # Boundary conditions
        u_jp1[0] = u_b[0]; u_jp1[mx] = u_b[1]
            
        # Update u_j
        u_j[:] = u_jp1[:]
        
    return u_j

def parabolic_pde(method,coeff,L,T,u0,u_b,mx,mt):
    '''This function solves the heat (parabolic PDE) 
    e.g.  u_t = coeff*u_xx  0<x<L, 0<t<T
          with temperature boundary conditions
          u=u_b at x=0,L, t>0
          and prescribed initial temperature
          u=u0(x) 0<=x<=L,t=0
    equation by returning u(x,t) and x. The variables consist of 
    method, the method to use between forwards or backwards Euler 
    methods and the Nicholson method; coeff, the diﬀusion coeﬃcient κ > 0;
    L, the size of the domain; T, the desired computation time;
    u0, the initial temperature distribution; mx, the number of 
    discretisation points/grid spacings in space; finally mt, 
    the number of discretisation points/grid spacings in time.
    '''
    # set up the numerical environment variables
    x = np.linspace(0, L, mx+1)     # mesh points in space
    t = np.linspace(0, T, mt+1)     # mesh points in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
    lmbda = coeff*deltat/(deltax**2)    # mesh fourier number
    
    # set up the solution variables
    u_j = np.zeros(x.size)        # u at current time step
    u_jp1 = np.zeros(x.size)      # u at next time step
    
    ## Set initial condition
    for i in range(0, mx+1):
        u_j[i] = u0(x[i],L)
    
    U = method(lmbda,mx,mt,u_jp1,u_j,u_b)
    E_max = max(abs(U-u_exact(x,T)))
    
    return E_max,deltax,deltat
   
def E_order_delt(method,p):
    # number of gridpoints in space
    mx = 200
    #varying number of gridpoints in time
    mt = [150,160,170,180,190,200]
    deltat = []
    E = []
    for i in mt:
        Emax, delx, delt = parabolic_pde(method,p[0],p[1],p[2],p[3],p[4],mx,i)
        deltat.append(abs(np.log10(delt)))
        E.append(abs(np.log10(Emax)))
        
    return E, deltat

def E_order_delx(method,p):
    #varying number of gridpoints in space
    mx = [2,3,4,5,6,7,8,9,10,11,12,13,14,15] 
    #number of gridpoints in time
    mt = 1500   
    deltax = []
    E = []
    for i in mx:
        Emax, delx, delt = parabolic_pde(method,p[0],p[1],p[2],p[3],p[4],i,mt)
        deltax.append(abs(np.log10(delx)))
        E.append(abs(np.log10(Emax)))
        
    return E, deltax
     
'''main function'''
if __name__ == "__main__":
    '''set problem parameters/functions'''
    kappa = 1.0   # diffusion constant
    L=1.0         # length of spatial domain
    T=0.5        # total time to solve for
    u_b = [0,0]   #boundary conditions values (u_b[0] = value of u(0,T) and u_b[1] = value of u(L,T))
    
    '''which method do you want to test?'''
#    method = forwards_Euler
    method = nicholson 
#    method = backwards_Euler
    pars = [kappa, L, T, u_I, u_b]
    
    '''Observing order of error in delta t'''
    E_delt, delt = E_order_delt(method, pars)
    
    '''Observing order of error in delta x'''
    E_delx, delx = E_order_delx(method, pars)

    '''Plotting to find the order of the error'''
    pl.figure(1)
    pl.plot(delt,E_delt,'bo-')
    pl.xlabel('log(delta t)'); pl.ylabel('log of Error, log(E)')
    pl.title('Log of truncation Error against delta t for the '+ method.__name__ +' method.')
    #pl.ylim([18,18.25])
    #pl.yscale('log'); pl.xscale('log')
    pl.figure(2)
    pl.plot(delx,E_delx,'bo-')
    pl.xlabel('log(delta x)'); pl.ylabel('log of Error, log(E)')
    pl.title('Log of truncation Error against delta x for the '+ method.__name__ +' method.')
    #pl.ylim([18,18.25])
    #pl.yscale('log'); pl.xscale('log')
    pl.show