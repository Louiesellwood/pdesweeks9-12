'''The Elliptic solver only works with Dirichlet Boundary conditions
   and uses an iteration scheme method that gives the user a choice 
   of using a Jacobi iteration or Successive over relaxation (SOR) method.
'''
import numpy as np
from math import pi
import pylab as pl
from scipy.optimize import minimize_scalar as minimise

'''Exact solution'''
def u_exact(x,y):
    # the exact solution
    y = np.sin(pi*x/Lx)*np.sinh(pi*y/Lx)/np.sinh(pi*Ly/Lx)
    return y

'''iteration scheme to evaluate the solution'''
def iteration_scheme(w,p):
    #unpack parameters
    method,maxerr,mx,my = p
    
    # set up the numerical environment variables
    x = np.linspace(0, Lx, mx+1)     # mesh points in x
    y = np.linspace(0, Ly, my+1)     # mesh points in y
    deltax = x[1] - x[0]             # gridspacing in x
    deltay = y[1] - y[0]             # gridspacing in y
    lambdasqr = (deltax/deltay)**2       # mesh number
    
    # set up the solution variables
    u_old = np.zeros((x.size,y.size))   # u at current time step
    u_new = np.zeros((x.size,y.size))   # u at next time step
    Res = np.zeros((x.size,y.size))  # residue
    
    # intialise the boundary conditions, for both timesteps
    u_old[1:-1,0] = fB(x[1:-1])
    u_old[1:-1,-1] = fT(x[1:-1])
    u_old[0,1:-1] = fL(y[1:-1])
    u_old[-1,1:-1] = fR(y[1:-1])
    u_new[:]=u_old[:]
    
    # true solution values on the grid 
    u_true = np.zeros((x.size,y.size))  # exact solution
    for i in range(0,mx+1):
        for j in range(0,my+1):
            u_true[i,j] = u_exact(x[i],y[j])
 
    # initialise the iteration
    err = maxerr+1
    count = 1
    while err>maxerr and count<maxcount:
        for j in range(1,my):
            for i in range(1,mx):
                if method == "SOR_method":
                    #defining residue 
                    Res[i,j] = ( u_new[i-1,j] + u_old[i+1,j] - ((2*(1+lambdasqr))*u_old[i,j]) \
                       + lambdasqr*(u_new[i,j-1] + u_old[i,j+1]) )/(2*(1+lambdasqr))
                    #evaluating u_new using omega (Successive over relaxation)
                    u_new[i,j] = u_old[i,j] + w*Res[i,j]
                elif method == "Jacobi_it_method":
                    #evaluating u_new using iterative solution
                    u_new[i,j] = ( u_old[i-1,j] + u_old[i+1,j] + \
                    lambdasqr*(u_old[i,j-1]+u_old[i,j+1]) )/(2*(1+lambdasqr))
                elif method == "Gauss_Seidel":
                    u_new[i,j] = ( u_new[i-1,j] + u_old[i+1,j] + \
                    lambdasqr*(u_new[i,j-1]+u_old[i,j+1]) )/(2*(1+lambdasqr))
                else:
                    assert(method != "SOR_method" or method != "Jacobi_it_method" \
                           or method != "Gauss_Seidel"), "No such method, please \
                           check the method passed"
                
        err = np.max(np.abs(u_new-u_old))
        u_old[:] = u_new[:]
        count=count+1

    # calculate the error, compared to the true solution    
    err_true = np.max(np.abs(u_new[1:-1,1:-1]-u_true[1:-1,1:-1]))
    
    return count, err, err_true, u_new

def min_iterations(omega,p):
    count,err,err_true,u_new = iteration_scheme(omega,p)
    
    return count

def omega_for_min_it(f,p):
    min_w = minimise(lambda w: f(w,p),bounds=(1,2),method = 'bounded')
    #checks if the scheme converges
    assert(min_w.success == True), "The scipy.optimize.minimize_ \
    scalar didn't converge."
    return min_w.x
  
def varying_parameters(mx,my,maxerr,varying_par,index_vary_p):
    #initialising a solution vector
    min_w = []
    #iterating through the various x values
    for i in varying_par:
        p = ["SOR_method",maxerr,mx,my]
        p[index_vary_p] = i
        min_w.append(omega_for_min_it(min_iterations,p))
        
    return min_w

if __name__ == "__main__":
    '''Problem parameters and variables'''
    # set dimensions of spatial domain
    Lx=2.0 
    Ly=1.0    
    # maximum number of iteration steps
    maxcount = 1000
    
    '''Dirichlet Boundary Conditions'''
    def fB(x):
        # y=0 boundary condition
        u = np.zeros(x.size)
        return u
    
    def fT(x):
        # y=Ly boundary condition
        u = np.sin(pi*x/Lx)
        return u
    
    def fL(y):
        # x=0 boundary condition
        u = np.zeros(y.size)
        return u
    
    def fR(y):
        # x=Lx boundary condition
        u = np.zeros(y.size)
        return u
    
    '''Investigating parameter dependence of the omega value that causes
       the least amount of iterations.
    '''
    #p = ["SOR_method",maxerr,maxcount,mx,my]
    #varying the number of gridpoints in x       
    mx_vary = [20,40,60,80,100,150,200]
    mx_cons = 40
    index_mx = 2
    #varying the number of gridpoints in y
    #my_vary = [20,40,60,80,100,150,200] 
    my_vary = [300,350,400,450,500]
    my_cons = 20  
    index_my = 3      
    #varying the target error
    TE_vary = [1e-1,1e-2,1e-3,1e-4,1e-5,1e-6,1e-7]
    TE_cons = 1e-4
    index_TE = 1
    #solutions to the varying parameters
    #omega_mx = varying_parameters(mx_vary,my_cons,TE_cons,mx_vary,index_mx)
    omega_my = varying_parameters(mx_cons,my_vary,TE_cons,my_vary,index_my)
    #omega_TE = varying_parameters(mx_cons,my_cons,TE_vary,TE_vary,index_TE)
    
    '''Plotting to find the parameter dependence'''
#    pl.figure(1)
#    pl.plot(mx_vary,omega_mx,'bo')
#    pl.title('The dependence of the optimal value of omega on mx')
#    pl.xlabel('The number of gridpoints in x, mx'); pl.ylabel('Omega')
 
#    pl.figure(2)
#    pl.plot(TE_vary,omega_TE,'yo')
#    pl.title('The dependence of the optimal value of omega on the target error')
#    pl.xlabel('The target error'); pl.ylabel('Omega')
#    pl.xscale('log')

    pl.figure(3)
    pl.title('The dependence of the optimal value of omega on my')
    pl.plot(my_vary,omega_my,'ro')
    pl.xlabel('The number of gridpoints in y, my'); pl.ylabel('Omega')
    pl.show    