# simple explicit finite difference solver for the 1D wave equation
#   u_tt = c^2 u_xx  0<x<L, 0<t<T
# with zero-displacement boundary conditions
#   u=0 at x=0,L, t>0
# and prescribed initial displacement and velocity
#   u=u_I(x), u_t=v_I(x)  0<=x<=L,t=0

import numpy as np
import pylab as pl
from math import pi
from scipy.sparse import diags
from scipy.sparse.linalg import spsolve

'''Exact solutions'''
def u_exact(x,t,c,L):
    # the exact solution
    y = np.cos(pi*c*t/L)*np.sin(pi*x/L)
    return y  
    
#the different matrices for the different methods
def matrix(method, lmbda, mx):
    #returns a matrix depending on the method
    shape = (mx-1,mx-1)
    
    if method == "standard":
        x_diag = 2-(2*lmbda**2); yz_diag = lmbda**2
        A_be = diags([yz_diag,x_diag,yz_diag],[-1,0,1],shape)
        
        return A_be
    
    elif method == "stable 1":
        x_diag = 1+lmbda**2; yz_diag = -0.5*lmbda**2
        #B_iw is just the negation of A_iw
        A_iw = diags([yz_diag,x_diag,yz_diag],[-1,0,1],shape)
        B_iw = diags([-yz_diag,-x_diag,-yz_diag],[-1,0,1],shape)

        return A_iw, B_iw
    
    elif method == "stable 2":
        #Note the upper and lower diagonals are the same
        Ax_diag = 1+(0.5*lmbda**2); Ayz_diag = -0.25*lmbda**2
        #B_bw is just the negation of A_bw
        Cx_diag = 2-(lmbda**2); Cyz_diag = 0.5*lmbda**2        
        A_bw = diags([Ayz_diag,Ax_diag,Ayz_diag],[-1,0,1],shape)
        B_bw = diags([-Ayz_diag,-Ax_diag,-Ayz_diag],[-1,0,1],shape)
        C_bw = diags([Cyz_diag,Cx_diag,Cyz_diag],[-1,0,1],shape)

        return A_bw, B_bw, C_bw

def first_timestep(lmbda,deltat,x,u_j,u_jp1,u_jm1,mx):
    # First timestep
    for i in range(1,mx):
        u_j[i] = u_jm1[i] + 0.5*(lmbda**2)*(u_jm1[i-1] - 2*u_jm1[i] + u_jm1[i+1]) \
                + deltat*v_I(x[i])
    # First timestep boundary condition
    u_j[0] = 0; u_j[mx]=0
    
    return u_j

'''Conditionally stable method - with magic timestep'''
def standard(lmbda,x,deltax,deltat,mx,mt,u_jp1,u_j,u_jm1,p_j):
    #first timestep 
    u_jm1 = first_timestep(lmbda,deltat,x,u_j,u_jp1,u_jm1,mx)
    #return correct matrix for the standard method
    A_ew = matrix("standard",lmbda,mx)    
    # Solve the PDE: loop over time points excluding the first
    for i in range(2, mt+1):
        #BC's evaluated at each time step (may depend on time)
        p_j[0] = u_BL(i)
        p_j[-1] = u_BR(i)
        #linear solver solving simultaneous equation
        u_jp1[1:-1] = A_ew.dot(u_j[1:-1]) - u_jm1[1:-1] + (lmbda**2)*p_j
        # boundary conditions
        u_jp1[0] = p_j[0]; u_jp1[mx] = p_j[-1]
        # update u_jm1 and u_j
        u_jm1[:],u_j[:] = u_j[:],u_jp1[:]    
    
    return u_jp1
    
'''unconditionally stable method 1'''
def stable_method1(lmbda,x,deltax,deltat,mx,mt,u_jp1,u_j,u_jm1,p_j):
    #first timestep 
    u_j = first_timestep(lmbda,deltat,x,u_j,u_jp1,u_jm1,mx)
    #return correct matrix for the standard method
    A_iw, B_iw = matrix("stable 1",lmbda,mx)
    # Solve the PDE: loop over all time points excluding the first
    for i in range(2, mt+1):
        #BC's evaluated at each time step (may depend on time)
        p_j[0] = u_BL(i)
        p_j[-1] = u_BR(i)   
        #linear solver solving simultaneous equation
        u_jp1[1:-1] = spsolve(A_iw,(2*u_j[1:-1] + B_iw.dot(u_jm1[1:-1])\
                             + (lmbda**2)*p_j))
        # boundary conditions
        u_jp1[0] = p_j[0]; u_jp1[mx] = p_j[-1]
        # update u_jm1 and u_j
        u_jm1[:],u_j[:] = u_j[:],u_jp1[:]  
    
    return u_jp1
    
'''unconditionally stable method 2'''
def stable_method2(lmbda,x,deltax,deltat,mx,mt,u_jp1,u_j,u_jm1,p_j):
    #first timestep 
    u_j = first_timestep(lmbda,deltat,x,u_j,u_jp1,u_jm1,mx)
    #return correct matrix for the standard method
    A_bw, B_bw, C_bw = matrix("stable 2",lmbda,mx)
    # Solve the PDE: loop over all time points excluding the first
    for i in range(2, mt+1):
        #BC's evaluated at each time step (may depend on time)
        p_j[0] = 0.5*(u_BL(i-1) + u_BL(i+1))
        p_j[-1] = 0.5*(u_BR(i-1) + u_BR(i+1))
        #linear solver solving simultaneous equation
        u_jp1[1:-1] = spsolve(A_bw,(C_bw.dot(u_j[1:-1]) + B_bw.dot(u_jm1[1:-1]) \
                                + (lmbda**2)*p_j))
        # boundary conditions
        u_jp1[0] = p_j[0]; u_jp1[mx] = p_j[-1]
        # update u_jm1 and u_j
        u_jm1[:],u_j[:] = u_j[:],u_jp1[:]  
    
    return u_jp1

'''Wave equation solver'''
def hyperbolic_pde(method,c,L,T,mx,mt):
    '''This function solves the wave (hyperbolic PDE) 
    e.g.  u_tt = c^2*u_xx  0<x<L, 0<t<T
          with boundary conditions
          u=u_b at x=0,L, t>0
          and prescribed initial displacement and velocity
          u(x,0)=u_I(x) and u_t(x,0)=v_I(x) for  0<=x<=L
    equation by returning u(x,t) and x. The variables consist of 
    method, the method to use between a standard (conditionally stable lmbda <=1)
    method or a unconditionally stable method; c, the wavespeed c > 0;
    L, the size of the domain; T, the desired computation time;
    u0, the initial temperature distribution; P, the left hand side boundary
    function; Q, the right hand side boundary function; mx, the number of 
    discretisation points/grid spacings in space; finally mt, 
    the number of discretisation points/grid spacings in time.
    '''
    # Set up the numerical environment variables
    x = np.linspace(0, L, mx+1)     # gridpoints in space
    t = np.linspace(0, T, mt+1)     # gridpoints in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
    lmbda = c*deltat/deltax         # squared courant number
    print("lambda=",lmbda)
    
    # set up the solution variables
    u_jm1 = np.zeros(x.size)        # u at previous time step
    u_j = np.zeros(x.size)          # u at current time step
    u_jp1 = np.zeros(x.size)        # u at next time step
    p_j = np.zeros(mx-1)            #p vector for boundaries

    # Set initial condition
    for i in range(0, mx+1):
        u_jm1[i] = u_I(x[i],L)   

    U = method(lmbda,x,deltax,deltat,mx,mt,u_jp1,u_j,u_jm1,p_j)
        
    return x,U    
    
'''main function'''
if __name__ == "__main__":
    '''Problem parameters and functions'''
    # Set problem parameters/functions
    c=1.0         # wavespeed
    L=1.0         # length of spatial domain
    T=2.0         # total time to solve for
    # Set numerical parameters
    mx = 50    # number of gridpoints in space
    mt = 500     # number of gridpoints in time
    
    '''Initial conditions'''
    def u_I(x,L):
        # initial displacement
        y = np.sin(pi*x/L)
        return y
    
    def v_I(x):
        # initial velocity
        y = np.zeros(x.size)
        return y   
    
    '''boundary conditions values (P = value of u(0,T) 
       and Q = value of u(L,T))
    '''
    #left boundary condition, P  
    def u_BL(t):
        '''Dirichlet BC?'''
        #typ = "Dirichlet" 
        P = 0
        return P
    
    #right boundary condition, Q
    def u_BR(t):
        '''Dirichlet BC?'''
        #typ = "Dirichlet"
        Q = 0
        return Q
    
    '''which method to use?'''
#    method = standard
#    method = stable_method1
    method = stable_method2
    #extract results from the method function
    x, U = hyperbolic_pde(method,c,L,T,mx,mt)
    
    '''Plot the final result and exact solution'''
    pl.plot(x,U,'ro',label='numeric')
    xx = np.linspace(0,L,250)
    pl.plot(xx,u_exact(xx,T,c,L),'b-',label='exact')
    pl.xlabel('x')
    pl.ylabel('u(x,'+str(T)+')')
    pl.legend()
    pl.show

'''Note - It is possible when running the standard method to get 
absolutely no truncation. This happens when lmbda = 1. In order to
check this, you must uncomment the code below; which prints the
truncation error. It is many magnitudes of order smaller than the 
errors from the other methods.'''
#    E = u_exact(x,T,c,L) - U
#    print(E)