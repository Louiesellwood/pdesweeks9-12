import numpy as np
import pylab as pl
from scipy.sparse import diags

def first_timestep(A_ew,x,lmbda,deltat,h0,u_j,u_jm1):
    # First timestep in matrix form
    u_j = 0.5*np.dot(A_ew,u_jm1) + deltat*v_I(x)
    # First timestep boundary condition
    u_j[0] = h0; u_j[mx] = h0;
    
    return u_j

def standard(lmbda,x,deltax,deltat,h0,mx,mt,u_jp1,u_j,u_jm1):
    '''This method is conditionally stable and has zero truncation error when 
       lmbda is exactly 1
    '''    
    #matrix form
    A_ew = diags([lmbda**2,2-(2*lmbda**2),lmbda**2],[-1,0,1],shape=(mx-1,mx-1)).toarray()
    # First timestep
    u_j = first_timestep(x,lmbda,deltat,h0,u_j,u_jm1)
    # Solve the PDE: loop over all time points
    for i in range(2, mt+1):
        u_jp1[1:-1] = np.dot(A_ew,u_j[1:-1]) - u_jm1[1:-1] #+ lmbda*Dirch
            
        # boundary conditions (open BC's)
        u_jp1[0] = u_j[0] + lmbda*(u_j[1]-u_j[0])
        u_jp1[mx] = u_j[mx] - lmbda*(u_j[mx]-u_j[mx-1])
                
        # update u_jm1 and u_j
        u_jm1[:],u_j[:] = u_j[:],u_jp1[:]
    
    return u_j

def vary_wavespeed_matrix(deltat,deltax):
    delta = (deltat/deltax)**2
    #q_i-0.5 = h(i*deltax - 0.5*deltax)
    diag_m1 = np.zeros(mx+1)
    diag_0 = np.zeros(mx+1)
    diag_1 = np.zeros(mx+1)
    for i in range(0,mx+1):
        diag_m1[i] = delta*h(i*deltax - 0.5*deltax)
        diag_0[i] = 2- delta*(h(i*deltax + 0.5*deltax) + h(i*deltax - 0.5*deltax))
        diag_1[i] = delta*h(i*deltax + 0.5*deltax)
    A = np.diagflat(diag_m1[1:],-1) + np.diagflat(diag_0) + np.diagflat(diag_m1[:-1],1)
    
    return A

'''unconditionally stable method'''
def solver(h,x,deltax,deltat,h0,mx,mt,u_jp1,u_j,u_jm1):   
    '''new lmbda at the boundaries'''
    # c is the square root of h0
    lmbda = (deltat/deltax)*np.sqrt(h0)
    #matirx
    A_ew = vary_wavespeed_matrix(deltat,deltax)
    #first timestep
    u_j = first_timestep(A_ew,x,lmbda,deltat,h0,u_j,u_jm1)

    # Solve the PDE: loop over all time points after first time step
    status = "not reached"
    for i in range(2, mt+1):
        u_jp1 = np.dot(A_ew,u_j) - u_jm1
        
        # boundary conditions (open BC's)
        tol = h0 + 1e-4
        condition = abs(u_jp1[0])
        
        if condition > tol and status == "not reached":
            status = "reached"
        if tol > condition and status == "reached":
            status = "passed"
        
        if status == "reached" or status == "not reached":
            u_jp1[0] = u_j[0] + lmbda*(u_j[1]-u_j[0]) 
        elif status == "passed":
            u_jp1[0] = u_j[mx]
        
        u_jp1[mx] = u_j[mx] - lmbda*(u_j[mx]-u_j[mx-1])
                
        # update u_jm1 and u_j
        u_jm1[:],u_j[:] = u_j[:],u_jp1[:] 
        
    return u_j

def hyperbolic_pde(method,h,L,T,u0,h0,mx,mt):
    # Set up the numerical environment variables
    x = np.linspace(0, L, mx+1)     # gridpoints in space
    t = np.linspace(0, T, mt+1)     # gridpoints in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
    print("deltax=",deltax)
    print("deltat=",deltat)

    # set up the solution variables
    u_jm1 = np.zeros(x.size)        # u at previous time step
    u_j = np.zeros(x.size)          # u at current time step
    u_jp1 = np.zeros(x.size)        # u at next time step
    
    
    # Set initial condition
    for i in range(0, mx+1):
        u_jm1[i] = u_I(x[i],L)   

    U = method(h,x,deltax,deltat,h0,mx,mt,u_jp1,u_j,u_jm1)
        
    return x,U
    
'''main function'''
if __name__ == "__main__":
    '''Set problem parameters'''
    L=4       # length of spatial domain
    T= 3  # total time to solve for
    # Set numerical parameters
    mx = 500    # number of gridpoints in space
    mt = 10000    # number of gridpoints in time
    
    '''initial condition and boundary conditions'''   
    #the height of the sea surface
    h0 = 0.25
    #placement of initial wave
    placement_w = 0.3*L
    #size of the initial wave disturbance
    size_ss = 0.05
        #function for seabed
    def seabed(x):
        #want a flat surface (return roughly zero)
        return 0.00000001*np.exp(-((x - 5)**2)/(0.05*L)**2)    
    #the height of the sea bed
    def h(x):
        return 0.5*h0
    #the initial wave
    def u_I(x,L):
        # initial displacement
        y = 0.5*h0 + size_ss*np.exp(-((x - placement_w)**2)/(0.05*L)**2)
        return y
    #the initial wave velocity
    def v_I(x):
        # initial velocity
        y = np.zeros(x.size)
        return y

    '''solving wave equation'''
    x, U = hyperbolic_pde(solver,h,L,T,u_I,0.5*h0,mx,mt) 
    
    '''Plot the sea surface and seabed'''
    pl.plot(x,U+0.5*h0,'r-',label='sea surface')
    xx = np.linspace(0,L,250)
    pl.plot(xx,seabed(xx),'b-',label='seabed')
    pl.xlabel('x')
    pl.ylabel('u(x,'+str(T)+')')
    pl.legend()
    pl.show