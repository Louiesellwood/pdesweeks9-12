'''The Elliptic solver only works with Dirichlet Boundary conditions
   and uses an iteration scheme method that gives the user a choice 
   of using a Jacobi iteration or Successive over relaxation (SOR) method.
'''
import numpy as np
from math import pi
import matplotlib.pyplot as plt
from scipy.optimize import minimize_scalar as minimise

'''Dirichlet Boundary Conditions'''
def fB(x):
    # y=0 boundary condition
    u = np.zeros(x.size)
    return u

def fT(x):
    # y=Ly boundary condition
    u = np.sin(pi*x/Lx)
    return u

def fL(y):
    # x=0 boundary condition
    u = np.zeros(y.size)
    return u

def fR(y):
    # x=Lx boundary condition
    u = np.zeros(y.size)
    return u

'''Exact solution'''
def u_exact(x,y):
    # the exact solution
    y = np.sin(pi*x/Lx)*np.sinh(pi*y/Lx)/np.sinh(pi*Ly/Lx)
    return y

'''iteration scheme to evaluate the solution'''
def iteration_scheme(w,p):
    method,err,maxerr,count,maxcount,lambdasqr = p
    # solve the PDE
    # set up the solution variables
    u_old = np.zeros((x.size,y.size))   # u at current time step
    u_new = np.zeros((x.size,y.size))   # u at next time step
    Res = np.zeros((x.size,y.size))  # residue
    
    # intialise the boundary conditions, for both timesteps
    u_old[1:-1,0] = fB(x[1:-1])
    u_old[1:-1,-1] = fT(x[1:-1])
    u_old[0,1:-1] = fL(y[1:-1])
    u_old[-1,1:-1] = fR(y[1:-1])
    u_new[:]=u_old[:]
    
    while err>maxerr and count<maxcount:
        for j in range(1,my):
            for i in range(1,mx):
                if method == "SOR_method":
                    #defining residue 
                    Res[i,j] = ( u_new[i-1,j] + u_old[i+1,j] - ((2*(1+lambdasqr))*u_old[i,j]) \
                       + lambdasqr*(u_new[i,j-1] + u_old[i,j+1]) )/(2*(1+lambdasqr))
                    #evaluating u_new using omega (Successive over relaxation)
                    u_new[i,j] = u_old[i,j] + w*Res[i,j]
                elif method == "Jacobi_it_method":
                    #evaluating u_new using iterative solution
                    u_new[i,j] = ( u_old[i-1,j] + u_old[i+1,j] + \
                    lambdasqr*(u_old[i,j-1]+u_old[i,j+1]) )/(2*(1+lambdasqr))
                elif method == "Gauss_Seidel":
                    u_new[i,j] = ( u_new[i-1,j] + u_old[i+1,j] + \
                    lambdasqr*(u_new[i,j-1]+u_old[i,j+1]) )/(2*(1+lambdasqr))
                else:
                    assert(method != "SOR_method" or method != "Jacobi_it_method" \
                           or method != "Gauss_Seidel"), "No such method, please \
                           check the method passed"
                
        err = np.max(np.abs(u_new-u_old))
        u_old[:] = u_new[:]
        count=count+1

    # calculate the error, compared to the true solution    
    err_true = np.max(np.abs(u_new[1:-1,1:-1]-u_true[1:-1,1:-1]))
    
    return count, err, err_true, u_new

def min_iterations(omega):
    count,err,err_true,u_new = iteration_scheme(omega,p)
    
    return count

def omega_for_min_it(f):
    min_w = minimise(min_iterations,bounds=(1,2),method = 'bounded')
    #checks if the scheme converges
    assert(min_w.success == True), "The scipy.optimize.minimize_ \
    scalar didn't converge."
    return min_w.x
    
if __name__ == "__main__":
    '''Problem parameters and variables'''
    # set dimensions of spatial domain
    Lx=2.0 
    Ly=1.0
    
    # set numerical parameters
    mx = 40             # number of gridpoints in x
    my = 20           # number of gridpoints in y
    maxerr = 1e-4        # target error
    maxcount = 1000      # maximum number of iteration steps
    
    # set up the numerical environment variables
    x = np.linspace(0, Lx, mx+1)     # mesh points in x
    y = np.linspace(0, Ly, my+1)     # mesh points in y
    deltax = x[1] - x[0]             # gridspacing in x
    deltay = y[1] - y[0]             # gridspacing in y
    lambdasqr = (deltax/deltay)**2       # mesh number
    
    # true solution values on the grid 
    u_true = np.zeros((x.size,y.size))  # exact solution
    for i in range(0,mx+1):
        for j in range(0,my+1):
            u_true[i,j] = u_exact(x[i],y[j])
    
    # initialise the iteration
    err = maxerr+1
    count = 1
    
    '''Finding the value of omega that minimises the number of iterations'''
    #parameters for 
    p = ["SOR_method",err,maxerr,count,maxcount,lambdasqr]
    #using scipy.optimize.minimize_scalar to find the omega value that 
    #minimises the number of interations of the SOR method
    min_w = omega_for_min_it(min_iterations) 
    print('The omega value that minimises the number of interations for the SOR iteration method is ', min_w)
    
    '''Running the iteration scheme for the various methods'''
    #run iteration function for the SOR method
    #method = "SOR_method"
    #when the SOR method is used, the omega value that minimises
    #the number of iterations is chosen
    
    #run iteration function for the Jacobi method
    #method = "Jacobi_it_method"
    
    #run iteration function for the Gauss_Seidel method
    method = "Gauss_Seidel"
    p_m = [method,err,maxerr,count,maxcount,lambdasqr]
    count,err,err_true,u_new = iteration_scheme(min_w,p_m)
   
    # diagnostic output
    print(method)
    print('Final iteration error =',err)
    print('Iterations =',count)
    print('Max diff from true solution =',err_true)
    
    '''Plotting the resulting solution'''
    xx = np.append(x,x[-1]+deltax)-0.5*deltax  # cell corners needed for pcolormesh
    yy = np.append(y,y[-1]+deltay)-0.5*deltay
    plt.pcolormesh(xx,yy,u_new.T)
    cb = plt.colorbar()
    cb.set_label('u(x,y)')
    plt.xlabel('x'); plt.ylabel('y')
    plt.show()