import numpy as np
import pylab as pl
from scipy.sparse import diags
from scipy.sparse.linalg import spsolve

def nicholson(lmbda,deltax,mx,mt,u_jp1,u_j,p):
    #Crank-Nicholson 
    A_cn = diags([-(lmbda/2),1+lmbda,-(lmbda/2)],[-1,0,1],shape=(mx-1,mx-1)).toarray()
    B_cn = diags([lmbda/2,1-lmbda,lmbda/2],[-1,0,1],shape=(mx-1,mx-1)).toarray()
    for n in range(1, mt+1):
        # Backward Euler timestep at inner mesh points
        u_jp1[1:-1] = spsolve(A_cn,B_cn.dot(u_j[1:-1]))
        # Boundary conditions
        u_jp1[0] = p[0]; u_jp1[mx] = p[1]
        # Update u_j
        u_j[:] = u_jp1[:]
        
    return u_j

'''get everything working for backwards Euler and then a Design decision was
to choose an implicit (BE) rather than an explicit (FE) method because it releases
the stability constraint (i.e. k(deltaT/deltaX^2) < 1/2).'''        

def backwards_Euler(lmbda,deltax,mx,mt,u_jp1,u_j,p):
    #backwards Euler matrix
    A_be = diags([-lmbda,1+(2*lmbda),-lmbda],[-1,0,1],shape=(mx-1,mx-1)).toarray()
    
    for n in range(1, mt+1):
        # Backward Euler timestep at inner mesh points
        u_jp1[1:-1] = spsolve(A_be,(u_j[1:-1]+ lmbda*p)) 
    
        # Boundary conditions
        u_jp1[0] = p[0]; u_jp1[mx] = p[-1]
        
        # Update u_j
        u_jp = u_jp1
        u_j[:] = u_jp[:]
    
    return u_j
    
def forwards_Euler(lmbda,deltax,mx,mt,u_jp1,u_j,u_BL,u_BR):
    #fowards Euler scheme
    #type of BC's
    typL, u_bl = u_BL(0)
    typR, u_br = u_BR(0)
    
    print("the left boundary is of type", typL)
    print("the right boundary is of type", typR)
    #checking combination of Boundary conditions
    if typL == "Neumann" and typR == "Dirichlet":
        A_fe = diags([lmbda,1-(2*lmbda),lmbda],[-1,0,1],shape=(mx,mx)).toarray()
        A_fe[0,1] = 2*lmbda
        pj = np.zeros(mx)
        
        for n in range(1, mt+1):
            # Forward Euler timestep at all-1row mesh points
            P_j = u_BL(n)
            Q_j = u_BR(n)
            print(Q_j[1])
            pj[0] = -2*deltax*P_j[1]
            pj[-1] = Q_j[1]
            print(pj[0])
            #print(np.shape(pj))
            #print(np.shape(u_j))
            u_jp1[:-1] = np.dot(A_fe,u_j[:-1]) + lmbda*pj
            #known BC
            u_jp1[-1] = Q_j[1]
            
            # Update u_j
            u_j[:] = u_jp1[:]
            
    elif typR == "Neumann" and typL == "Dirichlet":
        A_fe = diags([lmbda,1-(2*lmbda),lmbda],[-1,0,1],shape=(mx,mx)).toarray()
        A_fe[-1,-2] = 2*lmbda
        pj = np.zeros(mx)
        #loop through time
        for n in range(1, mt+1):
            # Forward Euler timestep at all-1row mesh points
            P_j = u_BL(n)
            Q_j = u_BR(n)
            pj[0] = P_j[1]
            pj[-1] = 2*deltax*Q_j[1]
            u_jp1[:-1] = np.dot(A_fe,u_j[:-1]) + lmbda*pj
            #known BC
            u_jp1[0] = P_j[1]
            # Update u_j
            u_j[:] = u_jp1[:]
        
    elif typR == "Neumann" and typL == "Neumann":
        A_fe = diags([lmbda,1-(2*lmbda),lmbda],[-1,0,1],shape=(mx+1,mx+1)).toarray()
        A_fe[0,1] = 2*lmbda
        A_fe[-1,-2] = 2*lmbda
        pj = np.zeros(mx+1)
        #loop through time
        for n in range(1, mt+1):
            
            P_j = u_BL(n)
            Q_j = u_BR(n)
            pj[0] = -P_j[1]
            pj[-1] = Q_j[1]
            # Forward Euler timestep at all mesh points
            u_jp1 = np.dot(A_fe,u_j) + 2*lmbda*deltax*pj
            # Update u_j
            u_j[:] = u_jp1[:]
        
    else:
        A_fe = diags([lmbda,1-(2*lmbda),lmbda],[-1,0,1],shape=(mx-1,mx-1)).toarray()
        pj = np.zeros(mx-1)
        #loop through time
        for n in range(1, mt+1):
            #updating p vector
            P_j = u_BL(n)
            Q_j = u_BR(n)
            pj[0] = P_j[1]
            pj[-1] = Q_j[1]
            
            print(pj)
            print(pj.shape)
            # Forward Euler timestep at inner mesh points
            u_jp1[1:-1] = np.dot(A_fe,u_j[1:-1]) + lmbda*pj
            # Boundary conditions
            u_jp1[0] = pj[0]; u_jp1[mx] = pj[-1]
            # Update u_j
            u_j[:] = u_jp1[:]
        
    return u_j

def parabolic_pde(method,coeff,L,T,u_I,P,Q,mx,mt):
    '''This function solves the Heat equation (parabolic PDE) 
    e.g.  u_t = coeff*u_xx  0<x<L, 0<t<T
          with temperature boundary conditions
          u(0,t)=u_BL and u(L,t)=u_BR for t>0
          and prescribed initial temperature
          u(x,0)=u_I(x) 0<=x<=L,t=0
    The function returns u(x,t) and x. The function variables consist
    of method, the method to use between forwards or backwards Euler 
    methods and the Crank-Nicholson method; coeff, the diffusion coefficient 
    coeff > 0; L, the size of the domain; T, the desired computation time;
    u_I, the initial temperature distribution; P, the left hand side boundary
    function; Q, the right hand side boundary function; mx, the number of 
    discretisation points/grid spacings in space; finally mt, the number
    of discretisation points/grid spacings in time.
    '''
    # set up the numerical environment variables
    x = np.linspace(0, L, mx+1)     # mesh points in space
    t = np.linspace(0, T, mt+1)     # mesh points in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
    lmbda = coeff*deltat/(deltax**2)    # mesh fourier number
    print("deltax=",deltax)
    print("deltat=",deltat)
    print("lambda=",lmbda)
    ## Set initial condition
    # set up the solution variables
    u_j = np.zeros(x.size)        # u at current time step
    u_jp1 = np.zeros(x.size)      # u at next time step
    for i in range(0, mx+1):
        u_j[i] = u_I(x[i],L)
        
    U = method(lmbda,deltax,mx,mt,u_jp1,u_j,u_BL,u_BR)
        
    return x,U
        
'''main function'''
if __name__ == "__main__":
    ## set problem parameters/functions
    kappa = 1.0   # diffusion constant
    L=1.0         # length of spatial domain
    T=0.5      # total time to solve 
    #set numerical parameters
    mx = 10 # number of gridpoints in space
    mt = 500   # number of gridpoints in time
    
    '''initial temperature distribution'''
    def u_I(x,L):
        y = np.sin(np.pi*x/L)
        return y
    
    '''right hand side function'''
    def F(x,t):
        return 2*t
      
    '''boundary conditions values (P = value of u(0,T) 
       and Q = value of u(L,T))
    '''
    #left boundary condition, P  
    def u_BL(t):
        '''Dirichlet BC?'''
#        typ = "Dirichlet" 
#        P = 0.001   
    
        '''Neumann BC?'''
        typ = "Neumann"
        P = 0.02*t;
    
        return typ, P
    
    #right boundary condition, Q
    def u_BR(t):
        '''Dirichlet BC?'''
#        typ = "Dirichlet"
#        Q = 0   
    
        '''Neumann BC?'''
        typ = "Neumann"
        Q = -0.02*t;
        
        return typ, Q
    
    '''solving heat equation'''
    x, U = parabolic_pde(forwards_Euler,kappa,L,T,u_I,u_BL,u_BR,mx,mt)
    
    '''plotting the final result and exact solution'''
    pl.plot(x,U,'-or',label='num')
    #x = np.linspace(0,L,250)
    #pl.plot(xx,u_exact(xx,T),'b-',label='exact')
    pl.xlabel('x')
    pl.ylabel('u(x,0.5)')
    pl.legend(loc='upper right')
    pl.show