import numpy as np
import pylab as pl
from scipy.sparse import diags
from scipy.sparse.linalg import spsolve

def nicholson(lmbda,deltax,mx,mt,u_jp1,u_j):
    #Crank-Nicholson 
    A_cn = diags([-(lmbda/2),1+lmbda,-(lmbda/2)],[-1,0,1],shape=(mx-1,mx-1)).toarray()
    B_cn = diags([lmbda/2,1-lmbda,lmbda/2],[-1,0,1],shape=(mx-1,mx-1)).toarray()
    for n in range(1, mt+1):
        # Backward Euler timestep at inner mesh points
        u_jp1[1:-1] = spsolve(A_cn,B_cn.dot(u_j[1:-1]))
        # Boundary conditions
        u_jp1[0] = u_BL(); u_jp1[mx] = u_BR()
        # Update u_j
        u_j[:] = u_jp1[:]
        
    return u_j

def ret_function_and_pvec(typR,typL,mx):
    #checking combination of Boundary conditions
    if typL == "Neumann" or typR == "Neumann":
        pj = np.zeros(mx)
        Fj = np.zeros(mx)       # right-hand-side function
        shape = (mx,mx)
        if typL == "Neumann" and typR == "Neumann":
            shape = (mx+1,mx+1)
            pj = np.zeros(mx+1)
            Fj = np.zeros(mx+1)       # right-hand-side function
            typ_BC = "NN"
        elif typL == "Neumann" and typR == "Dirichlet":
            typ_BC = "ND"
        else:
            typ_BC = "DN"
    else:
        typ_BC = "DD"
        shape = (mx-1,mx-1)
        pj = np.zeros(mx-1)
        Fj = np.zeros(mx-1)       # right-hand-side function
    
    return pj,Fj,typ_BC,shape

def ret_correct_matrix(typL,typR,method,lmbda,mx):
    #get the correct vectors for the different methods
    pj,Fj,typ_BC,shape = ret_function_and_pvec(typR,typL,mx)
    
    #general matrix form for the Forward or Backwards Euler scheme
    if method == "forwards_Euler":
        #forward Euler matrix
        A = diags([lmbda,1-(2*lmbda),lmbda],[-1,0,1],shape).toarray()
    else:
        #backward Euler matrix
        A = diags([-lmbda,1+(2*lmbda),-lmbda],[-1,0,1],shape).toarray()
    #adjusting the matrix for the different BC cases
    if typ_BC == "NN":
        if method == "forwards_Euler":
            A[0,1] = 2*lmbda
            A[-1,-2] = 2*lmbda
        else: 
            A[0,1] = -2*lmbda
            A[-1,-2] = -2*lmbda
    elif typ_BC == "ND":
        if method == "forwards_Euler":
            A[0,1] = 2*lmbda
        else: 
            A[0,1] = -2*lmbda
    elif typ_BC == "DN":
        if method == "forwards_Euler":
            A[-1,-2] = 2*lmbda
        else:
            A[-1,-2] = -2*lmbda
    else:
        pass
    
    return pj,Fj,typ_BC,A
    
def Euler(method,lmbda,deltax,deltat,mx,mt,u_jp1,u_j):
    #type of BC's
    typL, u_bl = u_BL(0)
    typR, u_br = u_BR(0)
    print("the left boundary is of type", typL)
    print("the right boundary is of type", typR)

    pj,Fj,typ_BC,A = ret_correct_matrix(typL,typR,method,lmbda,mx)

    #loop through time steps
    for n in range(1, mt+1):
        # Forward Euler timestep
        #BC's evaluated at each time step (may depend on time)
        P_j = u_BL(n)
        Q_j = u_BR(n)
        #Right-hand-side function F(x,t)
        for i in range(len(Fj)):
            Fj[i] = F((deltax*i),n)
            
        if typ_BC == "NN":
            pj[0] = -2*deltax*P_j[1]
            pj[-1] = 2*deltax*Q_j[1]
            if method == "forwards_Euler":
                u_jp1 = A.dot(u_j) + lmbda*pj + deltat*Fj
            else:
                u_jp1 = spsolve(A,(u_j + lmbda*pj + deltat*Fj))
            #no known BC's
        elif typ_BC == "ND":
            pj[0] = -2*deltax*P_j[1]
            pj[-1] = Q_j[1]
            if method == "forwards_Euler":
                u_jp1[:-1] = A.dot(u_j[:-1]) + lmbda*pj + deltat*Fj
            else:
                u_jp1[:-1] = spsolve(A,(u_j[:-1] + lmbda*pj + deltat*Fj))
            #known BC
            u_jp1[-1] = Q_j[1]
        elif typ_BC == "DN":
            pj[0] = P_j[1]
            pj[-1] = 2*deltax*Q_j[1]
            if method == "forwards_Euler":
                u_jp1[1:] = A.dot(u_j[1:]) + lmbda*pj + deltat*Fj
            else:
                u_jp1[1:] = spsolve(A,(u_j[1:] + lmbda*pj + deltat*Fj))
            #known BC
            u_jp1[0] = P_j[1]
        else:
            pj[0] = P_j[1]
            pj[-1] = Q_j[1]
            if method == "forwards_Euler":
                u_jp1[1:-1] = A.dot(u_j[1:-1]) + lmbda*pj + deltat*Fj
            else:
                u_jp1[1:-1] = spsolve(A,(u_j[1:-1] + lmbda*pj + deltat*Fj))
            # Boundary conditions
            u_jp1[0] = pj[0]; u_jp1[mx] = pj[-1]
            
        # Update u_j
        u_j[:] = u_jp1[:]
    
    return u_j

def parabolic_pde(method,coeff,L,T,mx,mt):
    '''This function solves the Heat equation (parabolic PDE) 
    e.g.  u_t = coeff*u_xx  0<x<L, 0<t<T
          with temperature boundary conditions
          u(0,t)=u_BL and u(L,t)=u_BR for t>0
          and prescribed initial temperature
          u(x,0)=u_I(x) 0<=x<=L,t=0
    The function returns u(x,t) and x. The function variables consist
    of method, the method to use between forwards or backwards Euler 
    methods and the Crank-Nicholson method; coeff, the diffusion coefficient 
    coeff > 0; L, the size of the domain; T, the desired computation time;
    u_I, the initial temperature distribution; P, the left hand side boundary
    function; Q, the right hand side boundary function; mx, the number of 
    discretisation points/grid spacings in space; finally mt, the number
    of discretisation points/grid spacings in time.
    '''
    # set up the numerical environment variables
    x = np.linspace(0, L, mx+1)     # mesh points in space
    t = np.linspace(0, T, mt+1)     # mesh points in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
    lmbda = coeff*deltat/(deltax**2)    # mesh fourier number
    print("deltax=",deltax)
    print("deltat=",deltat)
    print("lambda=",lmbda)
    ## Set initial condition
    # set up the solution variables
    u_j = np.zeros(x.size)        # u at current time step
    u_jp1 = np.zeros(x.size)      # u at next time step
    for i in range(0, mx+1):
        u_j[i] = u_I(x[i],L)
    
    if method == "forwards_Euler" or method == "backwards_Euler":
        print("The desired method used is", method)
        U = Euler(method,lmbda,deltax,deltat,mx,mt,u_jp1,u_j)
    else:
        print("The desired method used is", method.__name__)
        U = method(lmbda,deltax,mx,mt,u_jp1,u_j)
        
    return x,U
        
'''main function'''
if __name__ == "__main__":
    '''set problem parameters/functions'''
    kappa = 1.0   # diffusion constant
    L=1.0         # length of spatial domain
    T=1     # total time to solve for
    #set numerical parameters
    mx = 50 # number of gridpoints in space
    mt = 2000  # number of gridpoints in time
    
    '''initial temperature distribution'''
    def u_I(x,L):
        y = np.sin(np.pi*x/L)
        return y
    
    '''right hand side function'''
    def F(x,t):
        return 0
      
    '''boundary conditions values (P = value of u(0,T) 
       and Q = value of u(L,T))
    '''
    #left boundary condition, P  
    def u_BL(t):
        '''Dirichlet BC?'''
#        typ = "Dirichlet" 
#        P = 0   
    
        '''Neumann BC?'''
        typ = "Neumann"
        P = 2;
    
        return typ, P
    
    #right boundary condition, Q
    def u_BR(t):
        '''Dirichlet BC?'''
#        typ = "Dirichlet"
#        Q = 0 
#    
        '''Neumann BC?'''
        typ = "Neumann"
        Q = 0;
        
        return typ, Q

    '''solving heat equation using Euler methods'''
#    x, U = parabolic_pde("forwards_Euler",kappa,L,T,mx,mt)
    x, U = parabolic_pde("backwards_Euler",kappa,L,T,mx,mt)
    
    '''solving heat equation using the Crank_Nicholson method.
       This only works for homogeneous Boundary conditions, I
       haven't implemented the coice of other BC's for this method.    
    '''
#    #left BC
#    def u_BL():
#        #homogeneous dirichlet BC
#        return 0
#    #right BC
#    def u_BR():
#        #homogeneous dirichlet BC
#        return 0
#    x, U = parabolic_pde(nicholson,kappa,L,T,mx,mt)
    
    '''plotting the final result and exact solution'''
    pl.plot(x,U,'-or',label='num')
    #x = np.linspace(0,L,250)
    #pl.plot(xx,u_exact(xx,T),'b-',label='exact')
    pl.xlabel('x')
    pl.ylabel('u(x,'+str(T)+')')
    pl.legend(loc='upper right')
    pl.show